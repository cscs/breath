find_package(KF6CoreAddons ${KF6_MIN_VERSION} REQUIRED)
find_package(KF6ConfigWidgets ${KF6_MIN_VERSION} REQUIRED)
find_package(KF6IconThemes ${KF6_MIN_VERSION} REQUIRED)
find_package(KF6Service ${KF6_MIN_VERSION} REQUIRED)
find_package(
  Qt6
  COMPONENTS DBus
  REQUIRED)
add_executable(breath2tobreath main.cpp)

target_link_libraries(breath2tobreath KF6::CoreAddons KF6::ConfigWidgets
                      KF6::IconThemes KF6::Service Qt6::DBus)

install(TARGETS breath2tobreath
        DESTINATION ${KDE_INSTALL_LIBDIR}/kconf_update_bin/)
install(FILES breath2tobreath.upd DESTINATION ${KDE_INSTALL_KCONFUPDATEDIR})
